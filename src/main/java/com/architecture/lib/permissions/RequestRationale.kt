package com.architecture.lib.permissions

abstract class RequestRationale: Throwable() {
    abstract val permissions: Array<out String>
}