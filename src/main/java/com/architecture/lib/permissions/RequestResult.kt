package com.architecture.lib.permissions

interface RequestResult {
    val permissions: Array<out String>
    val grantResults: IntArray
}