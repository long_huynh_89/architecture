package com.architecture.lib.permissions

import android.app.Activity
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import android.content.pm.PackageManager
import androidx.annotation.RestrictTo
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.core.content.ContextCompat
import androidx.collection.ArrayMap
import android.util.Log
import com.architecture.lib.BuildConfig
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.disposables.Disposable
import java.util.*
import kotlin.collections.ArrayList

object RxPermissions {

    private val map = ArrayMap<Int, RequestHolder>()

    @JvmStatic
    fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>,
            grantResults: IntArray) {
        map[requestCode]?.returnResult(permissions, grantResults)
        map.remove(requestCode)
    }

    @JvmStatic
    fun request(activity: FragmentActivity, vararg permissions: String,
                force: Boolean = false): Single<RequestResult> {
        var isGranted = true
        val size = if (permissions.isNotEmpty()) permissions.size else 1
        val permissionsResult = BooleanArray(size) { false }

        permissions.forEachIndexed { i, permission ->
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                isGranted = false
                permissionsResult[i] = false
            } else {
                permissionsResult[i] = true
            }
        }

        return if (!isGranted) {
            val requestCode = generateRequestCode()

            // cancel old request
            map.remove(requestCode)?.cancel()

            // create new request
            RequestHolder(
                    ViewModelProviders.of(activity),
                    permissions, requestCode, force)
                    .apply {
                        map[requestCode] = this
                    }
                    .process(activity, activity)
        } else {
            Single.just(Result(permissions, IntArray(
                    1) { PackageManager.PERMISSION_GRANTED }))
        }
    }

    @JvmStatic
    fun request(fragment: Fragment, vararg permissions: String,
                force: Boolean = false): Single<RequestResult> {
        val activity = fragment.activity ?: return Single.error(
                KotlinNullPointerException("Activity must not null"))

        var isGranted = true
        val size = if (permissions.isNotEmpty()) permissions.size else 1
        val permissionsResult = BooleanArray(size) { false }

        permissions.forEachIndexed { i, permission ->
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                isGranted = false
                permissionsResult[i] = false
            } else {
                permissionsResult[i] = true
            }
        }

        return if (!isGranted) {
            val requestCode = generateRequestCode()

            // cancel old request
            map.remove(requestCode)?.cancel()

            // create new request
            RequestHolder(
                    ViewModelProviders.of(fragment),
                    permissions, requestCode, force)
                    .apply {
                        map[requestCode] = this
                    }
                    .process(activity, fragment)
        } else {
            Single.just(Result(permissions, IntArray(
                    1) { PackageManager.PERMISSION_GRANTED }))
        }
    }

    fun isGranted(activity: FragmentActivity?, vararg permissions: String): Boolean {
        return activity?.let { ac ->
            var result = true

            permissions.forEach {
                if (ContextCompat.checkSelfPermission(ac, it)
                        != PackageManager.PERMISSION_GRANTED) {
                    result = false
                    return@forEach
                }
            }

            result
        } == true
    }

    fun isDeniedForever(activity: FragmentActivity?, vararg permissions: String): Boolean {
        if (activity == null) return true

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                return true
            }
        }

        return false
    }

    private fun generateRequestCode(): Int {
        return 42
    }

    private data class Result(
            override val permissions: Array<out String>,
            override val grantResults: IntArray
    ): RequestResult {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Result

            if (!Arrays.equals(permissions, other.permissions)) return false
            if (!Arrays.equals(grantResults, other.grantResults)) return false

            return true
        }
        override fun hashCode(): Int {
            var result = Arrays.hashCode(permissions)
            result = 31 * result + Arrays.hashCode(grantResults)
            return result
        }
    }

    private data class RationaleError(
            override val permissions: Array<out String>
    ): RequestRationale() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as RationaleError

            if (!Arrays.equals(permissions, other.permissions)) return false

            return true
        }

        override fun hashCode(): Int {
            return Arrays.hashCode(permissions)
        }
    }

    @RestrictTo(value = [RestrictTo.Scope.LIBRARY_GROUP])
    internal class RequestHolder(
            viewModelProvider: ViewModelProvider,
            private val permissions: Array<out String>,
            private val requestCode: Int,
            private val force: Boolean = false
    ) {
        private val viewModel = viewModelProvider
                .get(RequestHolderViewModel::class.java)

        private var emitter: SingleEmitter<RequestResult>? = null
        private var disposable: Disposable? = null

        private val observerSuccess by lazy {
            Observer<RequestResult> { result ->
                emitter?.let { emitter ->
                    if (!emitter.isDisposed) {
                        result?.let { emitter.onSuccess(it) }
                    }
                }
            }
        }
        private val observerError by lazy {
            Observer<Throwable> { error ->
                emitter?.let { emitter ->
                    if (!emitter.isDisposed) {
                        error?.let { emitter.onError(it) }
                    }
                }
            }
        }

        init {
            viewModel.add(this)
        }

        fun process(activity: Activity,
                    lifecycleOwner: LifecycleOwner): Single<RequestResult> {
            cancelRx()
            cancelLiveData()

            viewModel.liveDataResult.observe(
                    lifecycleOwner, observerSuccess)
            viewModel.liveDataError.observe(
                    lifecycleOwner, observerError)

            return processInternal(activity)
                    .doOnSubscribe { disposable = it }
                    .doFinally { finally() }
        }

        private fun processInternal(activity: Activity): Single<RequestResult> {
            return Single.create {
                val rationaleList = ArrayList<String>()

                emitter = it

                if (!force) {
                    permissions.forEach { permission ->
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                        activity, permission)) {
                            rationaleList.add(permission)
                        }
                    }
                }

                if (rationaleList.isNotEmpty()) {
                    viewModel.liveDataError.postValue(
                            RationaleError(rationaleList.toTypedArray()))
                }
                else if (!it.isDisposed) {
                    ActivityCompat.requestPermissions(
                            activity, permissions, requestCode)
                }
            }
        }

        fun returnResult(permissions: Array<out String>, grantResults: IntArray) {
            viewModel.liveDataResult.postValue(Result(permissions, grantResults))
        }

        fun cancel() {
            if (BuildConfig.DEBUG) {
                Log.i("architecture", "[RxPermissions] [RequestHolder] " +
                        "[cancel] requestCode = $requestCode, permissions = $permissions")
            }

            cancelRx()
            cancelLiveData()

            viewModel.remove(this)
        }

        private fun cancelRx() {
            disposable?.let {
                if (!it.isDisposed) {
                    it.dispose()
                }
            }
            disposable = null
        }

        private fun cancelLiveData() {
            viewModel.liveDataResult.removeObserver(observerSuccess)
            viewModel.liveDataResult.postValue(null)

            viewModel.liveDataError.removeObserver(observerError)
            viewModel.liveDataResult.postValue(null)
        }

        private fun finally() {
            if (BuildConfig.DEBUG) {
                Log.i("architecture", "[RxPermissions] [RequestHolder] [finally] " +
                        "requestCode = $requestCode, permissions = $permissions, ")
            }

            cancelLiveData()
        }
    }

    @RestrictTo(value = [RestrictTo.Scope.LIBRARY_GROUP])
    class RequestHolderViewModel: ViewModel() {

        internal val liveDataResult by lazy {
            MutableLiveData<RequestResult>()
        }
        internal val liveDataError by lazy {
            MutableLiveData<Throwable>()
        }

        private val holders = ArrayList<RequestHolder>()
        private var isCleared = false

        internal fun add(holder: RequestHolder) {
            if (isCleared) {
                holder.cancel()
                return
            }

            if (!holders.contains(holder)) {
                holders.add(holder)
            }
        }

        internal fun remove(holder: RequestHolder) {
            if (isCleared) {
                return
            }

            if (holders.contains(holder)) {
                holders.remove(holder)
            }
        }

        override fun onCleared() {
            isCleared = true
            holders.forEach {
                it.cancel()
            }
            holders.clear()
        }
    }
}