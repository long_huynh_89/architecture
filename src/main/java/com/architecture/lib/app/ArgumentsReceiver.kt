package com.architecture.lib.app

import androidx.lifecycle.Observer

interface ArgumentsReceiver {
    fun registerRequestCodes(codes: List<Int>?)

    fun observe(observer: Observer<ArgumentsManager.Arguments>)
    fun removeObserver(observer: Observer<ArgumentsManager.Arguments>)
    fun removeObservers()
    fun cleanArguments()
}