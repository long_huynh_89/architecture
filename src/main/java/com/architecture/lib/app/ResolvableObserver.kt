package com.architecture.lib.app

/**
 * A simple callback that can receive from {@link ResolvableLiveData}.
 *
 * @param <T> The type of the parameter
 *
 * @see ResolvableLiveData LiveData - for a usage description.
 */
interface ResolvableObserver<in T> {

    /**
     * Called when the data is changed.
     * @param t  The new data
     * @return true if data is resolved
     */
    fun onChanged(t: T?): Boolean

    /**
     * Priority for call index
     * Biggest is called first
     */
    val priority: Int
}