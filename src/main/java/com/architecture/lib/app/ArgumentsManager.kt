package com.architecture.lib.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import java.lang.ref.WeakReference

object ArgumentsManager {

    private val pendingList by lazy {
        ArrayList<Receiver>()
    }

    fun createReceiver(fragment: Fragment): ArgumentsReceiver {
        val viewModel = ViewModelProviders.of(fragment,
                object : ViewModelProvider.Factory {
                    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                        @Suppress("UNCHECKED_CAST")
                        return InstanceViewModel(
                                this@ArgumentsManager,
                                fragment
                        ) as T
                    }
                }).get(InstanceViewModel::class.java)

        if (viewModel !in pendingList) {
            pendingList.add(viewModel)
        }

        return viewModel
    }

    fun createReceiver(activity: FragmentActivity): ArgumentsReceiver {
        val viewModel = ViewModelProviders.of(activity,
                object : ViewModelProvider.Factory {
                    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                        @Suppress("UNCHECKED_CAST")
                        return InstanceViewModel2(
                                this@ArgumentsManager,
                                activity
                        ) as T
                    }
                }).get(InstanceViewModel2::class.java)

        if (viewModel !in pendingList) {
            pendingList.add(viewModel)
        }

        return viewModel
    }

    fun setResult(requestCode: Int, resultCode: Int, data: Any? = null) {
        val arguments = Arguments(requestCode, resultCode, data)

        pendingList.forEach {
            if (it.requestCodes.contains(requestCode)) {
                it.result.postValue(arguments)
            }
        }
    }

    interface Receiver: ArgumentsReceiver {
        val requestCodes: List<Int>
        val result: MutableLiveData<Arguments>
    }

    private class InstanceViewModel(
            manager: ArgumentsManager,
            fragment: Fragment
    ): ViewModel(), Receiver {

        override val result = MutableLiveData<Arguments>()
        override val requestCodes = ArrayList<Int>()

        private val managerReference = WeakReference(manager)
        private val fragmentReference = WeakReference(fragment)

        override fun registerRequestCodes(codes: List<Int>?) {
            codes?.forEach {
                if (it !in requestCodes) {
                    requestCodes.add(it)
                }
            }
        }

        override fun observe(observer: Observer<Arguments>) {
            val fragment = fragmentReference.get() ?: return
            result.observe(fragment, observer)
        }

        override fun removeObserver(observer: Observer<Arguments>) {
            result.removeObserver(observer)
        }

        override fun removeObservers() {
            val fragment = fragmentReference.get() ?: return
            result.removeObservers(fragment)
        }

        override fun cleanArguments() {
            result.postValue(null)
        }

        override fun onCleared() {
            managerReference.get()
                    ?.pendingList
                    ?.remove(this)

            val fragment = fragmentReference.get()

            if (fragment != null) {
                result.removeObservers(fragment)
            }

            super.onCleared()
        }
    }

    private class InstanceViewModel2(
            manager: ArgumentsManager,
            activity: FragmentActivity
    ): ViewModel(), Receiver {

        override val result = MutableLiveData<Arguments>()
        override val requestCodes = ArrayList<Int>()

        private val managerReference = WeakReference(manager)
        private val activityReference = WeakReference(activity)

        override fun registerRequestCodes(codes: List<Int>?) {
            codes?.forEach {
                if (it !in requestCodes) {
                    requestCodes.add(it)
                }
            }
        }

        override fun observe(observer: Observer<Arguments>) {
            val activity = activityReference.get() ?: return
            result.observe(activity, observer)
        }

        override fun removeObserver(observer: Observer<Arguments>) {
            result.removeObserver(observer)
        }

        override fun removeObservers() {
            val activity = activityReference.get() ?: return
            result.removeObservers(activity)
        }

        override fun cleanArguments() {
            result.postValue(null)
        }

        override fun onCleared() {
            managerReference.get()
                    ?.pendingList
                    ?.remove(this)

            val activity = activityReference.get()

            if (activity != null) {
                result.removeObservers(activity)
            }

            super.onCleared()
        }
    }

    class Arguments(
            val requestCode: Int,
            val resultCode: Int,
            val data: Any? = null
    ) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Arguments

            if (requestCode != other.requestCode) return false
            if (resultCode != other.resultCode) return false
            if (data != other.data) return false

            return true
        }

        override fun hashCode(): Int {
            var result = requestCode
            result = 31 * result + resultCode
            result = 31 * result + (data?.hashCode() ?: 0)
            return result
        }

        override fun toString(): String {
            return "Arguments(requestCode=$requestCode, resultCode=$resultCode, data=$data)"
        }
    }
}