package com.architecture.lib.app

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.annotation.RestrictTo
import androidx.collection.ArrayMap
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import com.architecture.lib.BuildConfig
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean
import androidx.fragment.app.FragmentTransaction

@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
internal class BackStackManagerImpl
private constructor(): ViewModel(), BackStackManager, GenericLifecycleObserver {

    private var fragmentManagerReference: WeakReference<FragmentManager>? = null
    private var lifecycleReference: WeakReference<Lifecycle>? = null
    private var containerId = 0
    private var pendingAction: TransactionBuilder? = null
    private val index = count++

    private val isSavedState by lazy {
        AtomicBoolean(true)
    }
    private val isInit by lazy {
        AtomicBoolean(false)
    }
    private val callback by lazy {
        FragmentLifecycleCallbacks(this)
    }

    private fun init(
            fragmentManager: FragmentManager,
            lifecycle: Lifecycle, containerId: Int): Boolean {
        if (isInit.getAndSet(true)) {
            val fmManager = fragmentManagerReference?.get()
            val l = lifecycleReference?.get()

            if (fmManager != fragmentManager || l != lifecycle) {
                onCleared(fmManager, l)
            }
            else {
                return false
            }

            isSavedState.set(false)
        }
        if (BuildConfig.DEBUG) {
            Log.i(BackStackManager.LOG_TAG, "[#$index] [init] " +
                    "fragmentManager = ${fragmentManager.hashCode()}, " +
                    "lifecycle = ${lifecycle.hashCode()}")
        }

        fragmentManager.registerFragmentLifecycleCallbacks(callback, false)
        lifecycle.addObserver(this)

        fragmentManagerReference = WeakReference(fragmentManager)
        lifecycleReference = WeakReference(lifecycle)

        this.containerId = containerId
        return true
    }

    override fun popAll(newArgsForRoot: Bundle?) {
        pushFragment(PopTransactionBuilderImpl()
                .setArguments(newArgsForRoot))
    }

    override fun popTo(index: Int, newArgsForRoot: Bundle?) {
        pushFragment(PopTransactionBuilderImpl(
                index).setArguments(newArgsForRoot))
    }

    override fun pushFragment(fmClass: Class<out Fragment>, args: Bundle?, animEnter: Int?,
                              animExit: Int?, animPopEnter: Int?, animPopExit: Int?) {
        pushFragment(TransactionBuilderImpl().apply {
            setClass(fmClass, args)

            when {
                animEnter != null && animExit != null &&
                animPopEnter != null && animPopExit != null -> {
                    setCustomAnimations(animEnter,
                            animExit, animPopEnter, animPopExit)
                }

                animEnter != null && animExit != null -> {
                    setCustomAnimations(animEnter, animExit)
                }
            }
        })
    }

    override fun pushFragment(fm: Fragment, animEnter: Int?, animExit: Int?,
                              animPopEnter: Int?, animPopExit: Int?) {
        pushFragment(TransactionBuilderImpl().apply {
            setFragment(fm)

            when {
                animEnter != null && animExit != null &&
                animPopEnter != null && animPopExit != null -> {
                    setCustomAnimations(animEnter,
                            animExit, animPopEnter, animPopExit)
                }

                animEnter != null && animExit != null -> {
                    setCustomAnimations(animEnter, animExit)
                }
            }
        })
    }

    override fun pushFragment(builder: TransactionBuilder) {
        val isSavedState = this.isSavedState.get()
        val fragmentManager = fragmentManagerReference?.get()

        if (isSavedState || fragmentManager == null) {
            pendingAction = builder
        }
        else {
            (builder as TransactionBuilderImpl)
                    .build(fragmentManager, containerId)
                    ?.commit()
        }
    }

    override fun onStateChanged(source: LifecycleOwner?, event: Lifecycle.Event?) {
        val currentState = isSavedState.get()
        val function = Runnable {
            val state = fragmentManagerReference
                    ?.get()?.isStateSaved != false

            if (BuildConfig.DEBUG) {
                Log.i(BackStackManager.LOG_TAG, "[#$index] [onStateChanged] " +
                        "event = $event, isStateSaved = $state (previous = $currentState)")
            }

            isSavedState.set(state)

            if (currentState != state && !state) {
                executePendingActions()
            }
        }

        when (event) {
            Lifecycle.Event.ON_CREATE,
            Lifecycle.Event.ON_START,
            Lifecycle.Event.ON_RESUME -> {
                function.run()
            }

            else -> {
                // pending all transactions
                isSavedState.set(false)

                handler.post(function)
            }
        }
    }

    private fun executePendingActions() {
        val action = pendingAction

        if (action != null) {
            pendingAction = null
            pushFragment(action)
        }
    }

    private fun onFragmentAttached(f: Fragment) {
        if (BuildConfig.DEBUG) {
            Log.i(BackStackManager.LOG_TAG, "[#$index] [onFragmentAttached] " +
                    "fragment = ${f.javaClass.simpleName}, args = ${f.arguments}")
        }
    }

    private fun onFragmentDetached(f: Fragment) {
        if (BuildConfig.DEBUG) {
            Log.i(BackStackManager.LOG_TAG, "[#$index] [onFragmentDetached] " +
                    "fragment = ${f.javaClass.simpleName}, args = ${f.arguments}")
        }
    }

    private fun onCleared(fragmentManager: FragmentManager?,
                          lifecycle: Lifecycle?) {
        fragmentManager?.unregisterFragmentLifecycleCallbacks(callback)
        lifecycle?.removeObserver(this)
    }

    override fun onCleared() {
        if (BuildConfig.DEBUG) {
            Log.i(BackStackManager.LOG_TAG, "[#$index] [onCleared]")
        }

        onCleared(fragmentManagerReference
                ?.get(), lifecycleReference?.get())
    }

    companion object {
        private const val BACK_STACK_NAME = TransactionBuilder.backStackNameDefault

        private val handler = Handler(Looper.getMainLooper())
        private val factory = Factory()
        private var count = 0

        fun createBackStackManager(
                fragment: Fragment, id: Int): BackStackManagerImpl {
            val viewModel = ViewModelProviders.of(fragment,
                    factory).get(BackStackManagerImpl::class.java)

            if (viewModel.init(
                    fragment.childFragmentManager,
                    fragment.lifecycle, id) && BuildConfig.DEBUG) {
                Log.i(BackStackManager.LOG_TAG, "[createBackStackManager] " +
                        "${fragment.javaClass.simpleName} <= [${viewModel.index}]")
            }

            return viewModel
        }

        fun createBackStackManager(
                activity: FragmentActivity, id: Int): BackStackManagerImpl {
            val viewModel = ViewModelProviders.of(activity,
                    factory).get(BackStackManagerImpl::class.java)

            if (viewModel.init(
                    activity.supportFragmentManager,
                    activity.lifecycle, id) && BuildConfig.DEBUG) {
                Log.i(BackStackManager.LOG_TAG, "[createBackStackManager] " +
                        "${activity.javaClass.simpleName} <= [${viewModel.index}]")
            }

            return viewModel
        }
    }

    private class Factory: ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return BackStackManagerImpl() as T
        }
    }

    private class FragmentLifecycleCallbacks(
            parent: BackStackManagerImpl):
            FragmentManager.FragmentLifecycleCallbacks() {

        private val parentReference = WeakReference(parent)

        override fun onFragmentAttached(fm: FragmentManager, f: Fragment, context: Context) {
            parentReference.get()?.onFragmentAttached(f)
        }

        override fun onFragmentDetached(fm: FragmentManager, f: Fragment) {
            parentReference.get()?.onFragmentDetached(f)
        }
    }

    internal class PopTransactionBuilderImpl(
            private val index: Int? = null,
            private val backStackName: String? = BACK_STACK_NAME
    ): TransactionBuilderImpl() {
        override fun build(fm: FragmentManager, id: Int): FragmentTransaction? {
            if (index == null) {
                fm.popBackStack(backStackName,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
            else {
                val count = fm.backStackEntryCount
                val i = index + 1

                if (count > 0 && 0 <= i && i < count) {
                    val entry = fm.getBackStackEntryAt(i)

                    fm.popBackStack(entry.id,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
            }

            return null
        }
    }

    internal open class TransactionBuilderImpl: TransactionBuilder {

        private var backStackName: String? = BACK_STACK_NAME
        private var tag: String? = null
        private var targetFragment: Fragment? = null
        private var targetClass: Class<out Fragment>? = null
        private var targetArgs: Bundle? = null
        private val sharedElement by lazy {
            ArrayMap<String, WeakReference<View>>()
        }
        private var sharedElementEnterTransition: Any? = null
        private var sharedElementReturnTransition: Any? = null
        private var enterTransition: Any? = null
        private var exitTransition: Any? = null
        private var customAnimations2: Point? = null
        private var customAnimations4: Rect? = null

        override fun setFragment(fragment: Fragment): TransactionBuilder {
            targetFragment = fragment
            targetClass = null
            targetArgs = null

            return this
        }

        override fun setClass(jClass: Class<out Fragment>, args: Bundle?): TransactionBuilder {
            targetFragment = null
            targetClass = jClass
            targetArgs = args

            return this
        }

        override fun setArguments(args: Bundle?): TransactionBuilder {
            targetFragment?.let {
                it.arguments = args
            } ?:let {
                targetArgs = args
            }
            return this
        }

        override fun setBackStackName(value: String?): TransactionBuilder {
            backStackName = value
            return this
        }

        override fun setTag(value: String?): TransactionBuilder {
            tag = value
            return this
        }

        override fun setSharedElementEnterTransition(value: Any?): TransactionBuilder {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                sharedElementEnterTransition = value
            }

            return this
        }

        override fun setSharedElementReturnTransition(value: Any?): TransactionBuilder {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                sharedElementReturnTransition = value
            }

            return this
        }

        override fun addSharedElement(
                view: View, name: String): TransactionBuilder {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                sharedElement.put(name, WeakReference(view))
            }

            return this
        }

        override fun setEnterTransition(value: Any?): TransactionBuilder {
            enterTransition = value
            return this
        }

        override fun setExitTransition(value: Any?): TransactionBuilder {
            exitTransition = value
            return this
        }

        override fun setCustomAnimations(enter: Int, exit: Int): TransactionBuilder {
            customAnimations4 = null
            customAnimations2 = Point(enter, exit)

            return this
        }

        override fun setCustomAnimations(enter: Int, exit: Int, popEnter: Int,
                                         popExit: Int): TransactionBuilder {
            customAnimations2 = null
            customAnimations4 = Rect(enter, exit, popEnter, popExit)

            return this
        }

        private fun applyAnimation(
                transaction: FragmentTransaction,
                current: Fragment?, target: Fragment): FragmentTransaction {
            sharedElementEnterTransition?.let {
                target.sharedElementEnterTransition = it
            }

            enterTransition?.let {
                current?.enterTransition = it
                target.enterTransition = it
            }

            exitTransition?.let {
                current?.exitTransition = it
                target.exitTransition = it
            }

            customAnimations2?.let {
                transaction.setCustomAnimations(it.x, it.y)
            }

            customAnimations4?.let {
                transaction.setCustomAnimations(
                        it.left, it.top, it.right, it.bottom)
            }

            sharedElementReturnTransition?.let {
                target.sharedElementReturnTransition = it
            }

            sharedElement.forEach {
                val view = it.value.get()

                if (view != null) {
                    transaction.addSharedElement(view, it.key)
                }
            }

            return transaction
        }

        private fun applyBackStack(
                transaction: FragmentTransaction,
                isRootFragment: Boolean): FragmentTransaction {
            if (!isRootFragment && transaction.isAddToBackStackAllowed) {
                transaction.addToBackStack(backStackName)
            }

            return transaction
        }

        private fun createFragment(): Fragment {
            return (targetFragment ?: targetClass
                    ?.newInstance()?.apply {
                        arguments = targetArgs
                    })!!
                    .also {
                        if (it.arguments == null) {
                            it.arguments = Bundle()
                        }
                    }
        }

        open fun build(fm: FragmentManager, id: Int): FragmentTransaction? {
            val current = fm.findFragmentById(id)
            val isRootFragment = (current == null)
            val target = createFragment()

            return applyBackStack(applyAnimation(fm
                    .beginTransaction(), current, target)
                    .replace(id, target)
                    .setPrimaryNavigationFragment(target), isRootFragment)
        }
    }
}