package com.architecture.lib.app

import androidx.annotation.MainThread
import androidx.collection.ArrayMap
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

class CommunicationManager {

    private val mapValid by lazy {
        ArrayMap<String, Data>()
    }
    private val mapExpired by lazy {
        ArrayMap<String, WeakReference<Data>>()
    }
    private val mapLiveData by lazy {
        ArrayMap<String, WeakReference<ResolvableLiveData<Any>>>()
    }

    /**
     * Default Expire Time in Minutes
     */
    var defaultExpiredTime = 1L

    @MainThread
    fun addData(name: String, params: Any) {
        addData(name, params, defaultExpiredTime)
    }

    @MainThread
    fun addData(name: String, params: Any, expiredTime: Long) {
        mapExpired.remove(name)
        mapValid[name] = Data(name, params,
                expiredTime, System.currentTimeMillis())

        mapLiveData[name]?.get()?.value = params

        if (mapValid.size % COUNT_TO_CHECK_EXPIRED == 0) {
            clearExpiredData()
        }
    }

    @MainThread
    fun getData(name: String): ResolvableLiveData<Any> {
        return mapLiveData[name]?.get() ?: ResolvableLiveData<Any>().apply {
            mapLiveData[name] = WeakReference<ResolvableLiveData<Any>>(this)
            value = getDataPrivate(name)?.params
        }
    }

    private fun clearExpiredData() {
        mapValid.filter { !it.value.isExpired }
                .forEach {
                    mapValid.remove(it.key)
                    mapExpired[it.key] = WeakReference(it.value)
                }
    }

    private fun getDataPrivate(name: String): Data? {
        mapValid[name]?.let {
            if (it.isExpired) {
                mapValid.remove(name)
                mapExpired[name] = WeakReference(it)
            }
            return it
        }
        return mapExpired[name]?.get()
    }

    private class Data(
            val name: String,
            val params: Any,
            expiredTimeInMinutes: Long,
            val createdAt: Long) {

        private val expiredTimeInMilis = TimeUnit
                .MINUTES.toMillis(expiredTimeInMinutes)

        val isExpired: Boolean get() {
            val delta = System.currentTimeMillis() - createdAt
            return delta > expiredTimeInMilis
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Data

            if (name != other.name) return false
            if (params != other.params) return false
            if (expiredTimeInMilis != other.expiredTimeInMilis) return false

            return true
        }

        override fun hashCode(): Int {
            var result = name.hashCode()
            result = 31 * result + params.hashCode()
            result = 31 * result + expiredTimeInMilis.hashCode()
            return result
        }

        override fun toString(): String {
            return "Data(name='$name', params=$params, " +
                    "expiredTime=$expiredTimeInMilis, isExpired=$isExpired)"
        }
    }

    companion object {
        private const val COUNT_TO_CHECK_EXPIRED = 30
    }
}