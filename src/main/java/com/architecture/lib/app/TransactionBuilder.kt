package com.architecture.lib.app

import android.os.Bundle
import android.view.View
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.fragment.app.Fragment

interface TransactionBuilder {

    fun setFragment(fragment: Fragment): TransactionBuilder
    fun setClass(jClass: Class<out Fragment>, args: Bundle? = null): TransactionBuilder
    fun setArguments(args: Bundle?): TransactionBuilder

    fun setBackStackName(value: String?): TransactionBuilder
    fun setTag(value: String?): TransactionBuilder

    fun setSharedElementEnterTransition(value: Any?): TransactionBuilder
    fun setSharedElementReturnTransition(value: Any?): TransactionBuilder
    fun addSharedElement(view: View, name: String): TransactionBuilder

    fun setEnterTransition(value: Any?): TransactionBuilder
    fun setExitTransition(value: Any?): TransactionBuilder

    fun setCustomAnimations(
            @AnimatorRes @AnimRes enter: Int,
            @AnimatorRes @AnimRes exit : Int): TransactionBuilder

    fun setCustomAnimations(
            @AnimatorRes @AnimRes enter   : Int,
            @AnimatorRes @AnimRes exit    : Int,
            @AnimatorRes @AnimRes popEnter: Int,
            @AnimatorRes @AnimRes popExit : Int): TransactionBuilder

    companion object {
        const val backStackNameDefault = "BACK_STACK_NAME"

        fun create(): TransactionBuilder {
            return BackStackManagerImpl.TransactionBuilderImpl()
        }
    }
}