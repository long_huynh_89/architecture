package com.architecture.lib.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

interface BackStackManager {

    fun popTo(index: Int, newArgsForRoot: Bundle? = null)

    fun popAll(newArgsForRoot: Bundle? = null)

    @Deprecated("Too many params")
    fun pushFragment(fmClass: Class<out Fragment>, args: Bundle? = null,
                     animEnter: Int? = null, animExit: Int? = null,
                     animPopEnter: Int? = null, animPopExit: Int? = null)

    @Deprecated("Too many params")
    fun pushFragment(fm: Fragment,
                     animEnter: Int? = null, animExit: Int? = null,
                     animPopEnter: Int? = null, animPopExit: Int? = null)

    fun pushFragment(builder: TransactionBuilder)

    companion object {
        const val LOG_TAG = "BACK_STACK"

        fun getBackStackManager(fragment: Fragment, id: Int): BackStackManager {
            return BackStackManagerImpl.createBackStackManager(fragment, id)
        }
        fun getBackStackManager(activity: FragmentActivity, id: Int): BackStackManager {
            return BackStackManagerImpl.createBackStackManager(activity, id)
        }
    }
}