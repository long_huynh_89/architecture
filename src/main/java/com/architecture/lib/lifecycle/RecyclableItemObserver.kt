package com.architecture.lib.lifecycle

import android.view.View
import androidx.collection.ArrayMap
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import eu.davidea.flexibleadapter.items.IFlexible
import java.lang.ref.WeakReference

class RecyclableItemObserver<T>: ItemObserver {

    private val originRef: WeakReference<Observer<T>>
    private val manager: Manager?
    private val wrapper: ObserverWrapper<T>?

    constructor(origin: Observer<T>, data: LiveData<T>, fragment: Fragment) {
        originRef = WeakReference(origin)

        manager = try {
            ViewModelProviders
                    .of(fragment).get(Manager::class.java)
        } catch (e: Throwable) {
            null
        }

        wrapper = manager?.let { ObserverWrapper(originRef, it) }

        wrapper?.let { manager?.register(fragment, it, data) }
    }
    constructor(origin: Observer<T>, data: LiveData<T>, activity: FragmentActivity) {
        originRef = WeakReference(origin)

        manager = try {
            ViewModelProviders
                    .of(activity).get(Manager::class.java)
        } catch (e: Throwable) {
            null
        }

        wrapper = manager?.let { ObserverWrapper(originRef, it) }

        wrapper?.let { manager?.register(activity, it, data) }
    }

    override fun bindViewHolder(
            item: IFlexible<*>, view: View) {
        wrapper?.let { manager?.observe(it, item, view) }
    }

    override fun unbindViewHolder() {
        wrapper?.let { manager?.removeObserver(it) }
    }

    private class ObserverWrapper<T>(
            private val originRef: WeakReference<Observer<T>>,
            private val manager: Manager):
            Observer<T> {

        override fun onChanged(t: T) {
            val origin = originRef.get()

            if (origin == null) {
                manager.unregister(this)
            } else {
                origin.onChanged(t)
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ObserverWrapper<*>

            if (originRef != other.originRef) return false
            if (manager != other.manager) return false

            return true
        }

        override fun hashCode(): Int {
            var result = originRef.hashCode()
            result = 31 * result + manager.hashCode()
            return result
        }
    }

    private class Reference<T>(
            private val owner: LifecycleOwner,
            private val data: LiveData<T>,
            private val observer: Observer<T>) {

        fun observe() {
            if (data.value != null) {
                observer.onChanged(data.value)
            }

            data.observe(owner, observer)
        }

        fun removeObserver() {
            data.removeObserver(observer)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Reference<*>

            if (owner != other.owner) return false
            if (data != other.data) return false
            if (observer != other.observer) return false

            return true
        }

        override fun hashCode(): Int {
            var result = owner.hashCode()
            result = 31 * result + data.hashCode()
            result = 31 * result + observer.hashCode()
            return result
        }
    }

    class Manager: ViewModel() {

        private var isCleared = false
        private val map by lazy {
            ArrayMap<Observer<*>, Reference<*>>()
        }
        private val active by lazy {
            ArrayMap<String, Reference<*>>()
        }

        fun <T> register(owner: LifecycleOwner,
                         observer: Observer<T>, data: LiveData<T>) {
            if (isCleared) return

            map[observer] = Reference(
                    owner, data, observer)
        }

        fun <T> observe(observer: Observer<T>, item: IFlexible<*>, view: View) {
            if (isCleared) return

            val reference = map[observer] ?: return
            val id = "${item.layoutRes}_${view.hashCode()}"

            active[id]?.removeObserver()
            active[id] = reference

            reference.observe()
        }

        fun <T> removeObserver(observer: Observer<T>) {
            if (isCleared) return

            val reference = map[observer] ?: return

            active.forEach { entry ->
                if (entry.value == reference) {
                    active.remove(entry.key)
                            ?.removeObserver()
                    return
                }
            }
        }

        fun <T> unregister(observer: Observer<T>) {
            val reference = map.remove(observer)

            if (reference != null) {
                active.filter { it.value == reference }
                        .forEach { active.remove(it.key) }
            }
        }

        override fun onCleared() {
            isCleared = true

            active.forEach { entry ->
                entry.value.removeObserver()
            }

            active.clear()
            map.clear()
        }
    }

    companion object {
        fun createEmpty(): ItemObserver {
            return object : ItemObserver {
                override fun bindViewHolder(
                        item: IFlexible<*>, view: View) {}
                override fun unbindViewHolder() {}
            }
        }
    }
}