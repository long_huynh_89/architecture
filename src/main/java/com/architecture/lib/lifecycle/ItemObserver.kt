package com.architecture.lib.lifecycle

import android.view.View
import eu.davidea.flexibleadapter.items.IFlexible

interface ItemObserver {
    fun bindViewHolder(
            item: IFlexible<*>, view: View)

    fun unbindViewHolder()
}