package com.architecture.lib.dialog

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.FragmentActivity

open class BaseDialogBuilder(
        private var jClass: Class<out BaseDialogFragment>,

        val priority: Long = 0L,
        val isSimilarDismiss: Boolean = false,

        private val isNotHideUntilDismiss: Boolean = false
) {

    val id: Long = count++

    private var arguments: Bundle? = null

    fun setClass(jClass: Class<out BaseDialogFragment>): BaseDialogBuilder {
        this.jClass = jClass
        return this
    }

    fun setArguments(arguments: Bundle): BaseDialogBuilder {
        this.arguments = arguments
        return this
    }

    fun build(): BaseDialogFragment {
        val args = Bundle()
        arguments?.let { args.putAll(it) }

        args.putLong(ID, id)
        args.putLong(PRIORITY, priority)
        args.putBoolean(SIMILAR, isSimilarDismiss)
        args.putBoolean(NOT_HIDE, isNotHideUntilDismiss)

        val fragment = jClass.newInstance()
        fragment.arguments = args

        return fragment
    }

    fun show(activity: FragmentActivity?) {
        if (activity == null) return

        val viewModel = ViewModelProviders.of(activity)
                .get(BackStackDialogViewModel::class.java)

        viewModel.show(this)
    }

    fun hide(activity: FragmentActivity?) {
        if (activity == null) return

        val viewModel = ViewModelProviders.of(activity)
                .get(BackStackDialogViewModel::class.java)

        viewModel.hide(this)
    }

    fun dismiss(activity: FragmentActivity?) {
        if (activity == null) return

        val viewModel = ViewModelProviders.of(activity)
                .get(BackStackDialogViewModel::class.java)

        viewModel.dismiss(id)
    }

    open fun isSimilarTo(other: BaseDialogBuilder): Boolean {
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseDialogBuilder

        if (jClass != other.jClass) return false
        if (priority != other.priority) return false
        if (isSimilarDismiss != other.isSimilarDismiss) return false
        if (id != other.id) return false
        if (arguments != other.arguments) return false

        return true
    }

    override fun hashCode(): Int {
        var result = jClass.hashCode()
        result = 31 * result + priority.hashCode()
        result = 31 * result + isSimilarDismiss.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + (arguments?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "BaseDialogBuilder(jClass=${jClass.simpleName}, priority=$priority, " +
                "isSimilarDismiss=$isSimilarDismiss, id=$id, arguments=$arguments)"
    }

    companion object {
        private var count = 0L

        const val ID       = "BaseDialogFragment_ID"
        const val PRIORITY = "BaseDialogFragment_PRIORITY"
        const val SIMILAR  = "BaseDialogFragment_SIMILAR"
        const val NOT_HIDE = "BaseDialogFragment_NOT_HIDE"
    }
}