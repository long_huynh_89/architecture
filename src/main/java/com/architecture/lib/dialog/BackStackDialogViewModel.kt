package com.architecture.lib.dialog

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.SortedList

class BackStackDialogViewModel: ViewModel(), BackStackDialogFragment.ViewModel {

    override val builder by lazy {
        MutableLiveData<BaseDialogBuilder>()
    }
    private val builderList by lazy {
        SortedList<BaseDialogBuilder>(
                BaseDialogBuilder::class.java,
                Callback())
    }

    @Synchronized
    fun show(builder: BaseDialogBuilder) {
        builderList.add(builder)
    }

    @Synchronized
    fun hide(builder: BaseDialogBuilder) {
        val inListBuilder = findBuilder(builder.id)

        if (inListBuilder != null) {
            builderList.beginBatchedUpdates()
            builderList.remove(inListBuilder)

            if (inListBuilder.isSimilarDismiss) {
                removeSimilar(inListBuilder)
            }

            builderList.endBatchedUpdates()
        }
        else if (builder.isSimilarDismiss) {
            builderList.beginBatchedUpdates()
            removeSimilar(builder)
            builderList.endBatchedUpdates()
        }
    }

    @Synchronized
    fun dismiss(id: Long?) {
        val builder = findBuilder(id)

        if (builder != null) {
            builderList.beginBatchedUpdates()
            builderList.remove(builder)

            if (builder.isSimilarDismiss) {
                removeSimilar(builder)
            }

            builderList.endBatchedUpdates()
        }
    }

    @Synchronized
    fun getBuilder(id: Long?): BaseDialogBuilder? {
        return findBuilder(id)
    }

    private fun findBuilder(id: Long?): BaseDialogBuilder? {
        val size = builderList.size()
        if (size == 0) return null

        (0..(size - 1)).forEach { i ->
            val dialog = builderList[i]

            if (dialog.id == id) {
                return dialog
            }
        }

        return null
    }

    private fun removeSimilar(builder: BaseDialogBuilder) {
        var stopped = false

        while (!stopped) {
            val similarBuilder = findSimilarTo(builder)

            if (similarBuilder != null) {
                builderList.remove(similarBuilder)
            }
            else {
                stopped = true
            }
        }
    }

    private fun findSimilarTo(builder: BaseDialogBuilder): BaseDialogBuilder? {
        val size = builderList.size()
        if (size == 0) return null

        (0..(size - 1)).forEach { i ->
            val dialog = builderList[i]

            if (builder.isSimilarTo(dialog)) {
                return dialog
            }
        }

        return null
    }

    private inner class Callback: SortedList.Callback<BaseDialogBuilder>() {

        override fun areItemsTheSame(
                p0: BaseDialogBuilder?,
                p1: BaseDialogBuilder?): Boolean {
            return p0?.id == p1?.id
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {}

        override fun onChanged(position: Int, count: Int) {}

        override fun onInserted(position: Int, count: Int) {
            if (position == 0) {
                builder.postValue(builderList.get(0))
            }
        }

        override fun onRemoved(position: Int, count: Int) {
            if (position == 0) {
                val size = builderList.size()

                if (size != 0) {
                    builder.postValue(builderList.get(0))
                }
                else {
                    builder.postValue(null)
                }
            }
        }

        override fun compare(
                p0: BaseDialogBuilder?,
                p1: BaseDialogBuilder?): Int {
            return (-(p0?.priority ?: 0L) + (p1?.priority ?: 0L)).toInt()
        }

        override fun areContentsTheSame(
                p0: BaseDialogBuilder?,
                p1: BaseDialogBuilder?): Boolean {
            return p0?.id == p1?.id
        }
    }
}