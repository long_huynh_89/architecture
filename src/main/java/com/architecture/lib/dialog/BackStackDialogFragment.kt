package com.architecture.lib.dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.util.Log
import com.architecture.lib.BuildConfig

class BackStackDialogFragment : Fragment() {

    private val viewModel by lazy {
        activity?.let {
            ViewModelProviders.of(it).get(
                    BackStackDialogViewModel::class.java) as ViewModel
        }
    }
    private val child: BaseDialogFragment? get() {
        return childFragmentManager.findFragmentByTag(
                TAG_CHILD) as? BaseDialogFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel?.builder?.observe(this, Observer {
            if (BuildConfig.DEBUG) {
                Log.e("DEBUG", "[BackStackDialogFragment] " +
                        "[onCreate] [builder.observe] $it")
            }

            if (it != null) {
                processDialogBuilder(it)
            }
            else if (childFragmentManager.fragments.size > 0) {
                childFragmentManager.popBackStack(BACK_STACK,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        })
    }

    private fun processDialogBuilder(builder: BaseDialogBuilder) {
        if (BuildConfig.DEBUG) {
            Log.e("DEBUG", "[BackStackDialogFragment] " +
                    "[processDialogBuilder] [beforeCheck] builder = $builder")
        }

        if (builder != viewModel?.builder?.value) return

        if (BuildConfig.DEBUG) {
            Log.e("DEBUG", "[BackStackDialogFragment] " +
                    "[processDialogBuilder] [afterCheck] builder = $builder")
        }

        val child = this.child

        if (child?.id != builder.id) {
            var isRemove = false

            val transaction = childFragmentManager
                    .beginTransaction()
                    .also {
                        if (child != null) {
                            for (fm in childFragmentManager.fragments) {
                                if ((fm as? BaseDialogFragment)?.
                                                isNotHideUntilDismiss != true) {
                                    it.remove(fm)
                                    isRemove = true
                                }
                            }
                        }

                        if (!isRemove) {
                            it.addToBackStack(BACK_STACK)
                        }
                    }

            if (!isRemove) {
                if (BuildConfig.DEBUG) {
                    Log.e("DEBUG", "[BackStackDialogFragment] " +
                            "[processDialogBuilder] showed")
                }

                builder.build().show(
                        transaction, TAG_CHILD)
            }
            else {
                if (BuildConfig.DEBUG) {
                    Log.e("DEBUG", "[BackStackDialogFragment] " +
                            "[processDialogBuilder] remove first")
                }

                transaction.runOnCommit {
                    processDialogBuilder(builder) }.commit()
            }
        } else if (BuildConfig.DEBUG) {
            Log.e("DEBUG", "[BackStackDialogFragment] " +
                    "[processDialogBuilder] dialog is exists, id = ${child.id}")
        }
    }

    override fun onDestroy() {
        viewModel?.builder
                ?.removeObservers(this)

        super.onDestroy()
    }

    interface ViewModel {
        val builder: LiveData<BaseDialogBuilder>
    }

    companion object {
        private const val TAG = "BackStackDialogFragment"
        private const val TAG_CHILD = "BackStackDialogFragment_Child"
        private const val BACK_STACK = "BackStackDialogFragment_BackStack"

        fun init(fragmentManager: FragmentManager?) {
            if (fragmentManager == null) return

            if (fragmentManager.findFragmentByTag(TAG) == null) {
                fragmentManager.beginTransaction()
                        .add(BackStackDialogFragment(), TAG)
                        .commit()
            }
        }
    }
}