package com.architecture.lib.dialog

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import android.view.ViewGroup
import android.view.WindowManager

class BaseDialog: AppCompatDialog {

    private val callback: Callback

    constructor(context: Context?, theme: Int, callback: Callback): super(context, theme) {
        this.callback = callback
    }
    constructor(context: Context?, callback: Callback): super(context) {
        this.callback = callback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callback.onCreateDialog(this, savedInstanceState)
    }

    interface Callback {
        fun onCreateDialog(dialog: Dialog, savedInstanceState: Bundle?)
    }
}