package com.architecture.lib.dialog

import android.app.Dialog
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.FragmentActivity
import androidx.appcompat.app.AppCompatDialogFragment
import com.architecture.lib.BuildConfig
import java.lang.IllegalStateException
import androidx.lifecycle.ViewModel as BaseVM

abstract class BaseDialogFragment: AppCompatDialogFragment(), BaseDialog.Callback {

    val id by lazy {
        arguments?.getLong(BaseDialogBuilder.ID)
    }
    val priority by lazy {
        arguments?.getLong(BaseDialogBuilder.PRIORITY)
    }
    val isNotHideUntilDismiss by lazy {
        arguments?.getBoolean(BaseDialogBuilder.NOT_HIDE) == true
    }

    protected var builder: BaseDialogBuilder? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        val activity = context as? FragmentActivity ?: this.activity

        if (activity != null) {
            val parentVM = ViewModelProviders.of(activity)
                    .get(BackStackDialogViewModel::class.java)
            val viewModel = ViewModelProviders.of(
                    this).get(ViewModel::class.java)

            viewModel.id = id
            viewModel.parentVM = parentVM

            builder = parentVM.getBuilder(id)
        }
        else {
            reportError(IllegalStateException(
                    "FragmentActivity == null"))
        }

        if (BuildConfig.DEBUG) {
            Log.e("DIALOG", "[BaseDialogFragment] [onAttach] builder = $builder")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BaseDialog(context, theme, this)
    }

    override fun onCreateDialog(dialog: Dialog, savedInstanceState: Bundle?) {
        dialog.setOnKeyListener { _, _, event ->
            if (event.keyCode == KeyEvent.KEYCODE_BACK) {
                if (event.action == KeyEvent.ACTION_UP) {
                    close()
                }

                true
            } else {
                true
            }
        }
    }

    open fun close() {
        builder?.dismiss(activity) ?: dismiss()
    }

    override fun dismiss() {
        builder?.dismiss(activity)
                ?: super.dismiss()
    }

    override fun dismissAllowingStateLoss() {
        builder?.dismiss(activity)
                ?: super.dismissAllowingStateLoss()
    }

    protected abstract fun reportError(error: Throwable)

    class ViewModel: BaseVM() {

        var id: Long? = null
        var parentVM: BackStackDialogViewModel? = null

        override fun onCleared() {
            parentVM?.dismiss(id)
        }
    }
}